This is a project to demonstrate an issue when animating an AVPlayerLayer layerClassed view in a UIPercentDrivenInteractiveTransition subclassed pop gesture handler.

When running the project, you can tap the video to transition to the detail viewcontroller, and tap back to see the desired effect. When using a popgesture to go back instead, things glitch out pretty severely.

The NavigationAnimator class handles the transitioning between views. The NavigationInteractor handles the popgesture and the percent driven animation.

The issue disappears when you comment out the playerlayer / player related things in the PlayerView class (so that essentially youre left with a UIView with a background color)
