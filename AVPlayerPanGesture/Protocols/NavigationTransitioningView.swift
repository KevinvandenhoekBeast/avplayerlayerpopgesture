//
//  NavigationTransitioningView.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit

protocol NavigationTransitioningView {
    func getTransitioningView<T: UIView>() -> T?
    func getTargetRect() -> CGRect?
    func receive(transitionedView: UIView)
    func allows(view: UIView) -> Bool
    
    func prepareForTransition(presenting: Bool)
    func animateInTransition(presenting: Bool)
    func endTransition(presenting: Bool, completed: Bool)
}
