//
//  UIView+Extension.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit

extension UIView {
    func pinEdgesToSuperviewEdges(to view: UIView) {
        func pin(edges: NSLayoutAttribute...) {
            edges.forEach { (edge) in
                let constraint = NSLayoutConstraint(item: self, attribute: edge, relatedBy: .equal, toItem: view, attribute: edge, multiplier: 1, constant: 0)
                view.addConstraint(constraint)
                constraint.isActive = true
            }
        }
        
        pin(edges: .bottom, .top, .right, .left)
    }
}
