//
//  DetailViewController.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var playerView: PlayerView?
    let containerView = UIView()
    
    var targetRect: CGRect { return CGRect(x: 110, y: 320, width: 240, height: 120) }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        view.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.8, alpha: 1)
        
        playerView = PlayerView()
        
        view.addSubview(containerView)
        containerView.frame = targetRect
        containerView.layer.borderColor = UIColor.white.cgColor
        containerView.layer.borderWidth = 2
        containerView.backgroundColor = .clear
    }
}

extension DetailViewController: NavigationTransitioningView {
    
    func getTransitioningView<T>() -> T? where T : UIView {
        return playerView as? T
    }
    
    func getTargetRect() -> CGRect? {
        return targetRect
    }
    
    func receive(transitionedView: UIView) {
        guard let receivedView = transitionedView as? PlayerView else { return }
        self.playerView?.removeFromSuperview()
        self.playerView = receivedView
        containerView.addSubview(receivedView)
        receivedView.frame = containerView.bounds
    }
    
    func allows(view: UIView) -> Bool {
        return type(of: view.self) == PlayerView.self
    }
    
    func prepareForTransition(presenting: Bool) {
        view.alpha = presenting ? 0 : 1
    }
    
    func animateInTransition(presenting: Bool) {
        view.alpha = presenting ? 1 : 0
    }
    
    func endTransition(presenting: Bool, completed: Bool) {
        view.alpha = 1
    }
}
