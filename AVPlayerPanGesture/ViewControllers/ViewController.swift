//
//  ViewController.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var playerView: PlayerView?
    var targetRect: CGRect { return CGRect(x: 50, y: 120, width: 200, height: 320) }
    let containerView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        view.backgroundColor = UIColor(red: 0.7, green: 0.8, blue: 0.9, alpha: 1)
        playerView = PlayerView()
        
        guard let playerView = playerView else { return }
        view.addSubview(containerView)
        containerView.frame = targetRect
        containerView.layer.borderColor = UIColor.white.cgColor
        containerView.layer.borderWidth = 2
        containerView.backgroundColor = .clear
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tappedContainerView)))
        
        containerView.addSubview(playerView)
        playerView.frame = containerView.bounds
        playerView.backgroundColor = .red
        
        view.layoutSubviews()
    }
    
    @objc func tappedContainerView() {
        navigationController?.pushViewController(DetailViewController(), animated: true)
    }
}

extension ViewController: NavigationTransitioningView {
    
    func getTransitioningView<T>() -> T? where T : UIView {
        return playerView as? T
    }
    
    func getTargetRect() -> CGRect? {
        return targetRect
    }
    
    func receive(transitionedView: UIView) {
        guard let receivedView = transitionedView as? PlayerView else { return }
        self.playerView = receivedView
        containerView.addSubview(receivedView)
        receivedView.frame = containerView.bounds
    }
    
    func allows(view: UIView) -> Bool {
        return type(of: view.self) == PlayerView.self
    }
    
    func prepareForTransition(presenting: Bool) {
        // Nothing, optional
    }
    
    func animateInTransition(presenting: Bool) {
        // Nothing, optional
    }
    
    func endTransition(presenting: Bool, completed: Bool) {
        // Nothing, optional
    }
}
