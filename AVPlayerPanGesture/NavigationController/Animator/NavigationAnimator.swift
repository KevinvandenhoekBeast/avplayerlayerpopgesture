//
//  NavigationAnimator.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import Foundation
import UIKit

class NavigationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var duration: TimeInterval
    var isPresenting: Bool
    var originRect: CGRect
    var targetRect: CGRect
    var transitioningView: UIView
    var receivingView: NavigationTransitioningView
    var givingView: NavigationTransitioningView
    var navigationController: NavigationController
    
    init(duration: CGFloat, isPresenting: Bool, originRect: CGRect, targetRect: CGRect, transitioningView: UIView,
         givingView: NavigationTransitioningView, receivingView: NavigationTransitioningView, navigationController: NavigationController) {
        self.duration = Double(duration)
        self.isPresenting = isPresenting
        self.originRect = originRect
        self.targetRect = targetRect
        self.givingView = givingView
        self.receivingView = receivingView
        self.transitioningView = transitioningView
        self.navigationController = navigationController
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        guard let toVc = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let toTransitioningVc = toVc as? NavigationTransitioningView,
            let fromVc = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let fromTransitioningVc = fromVc as? NavigationTransitioningView,
            let fromView = fromVc.view,
            let toView = toVc.view else {
                return
        }
        
        if isPresenting {
            container.addSubview(fromView)
            container.addSubview(toView)
        } else {
            container.addSubview(toView)
            container.addSubview(fromView)
        }
        toView.frame = container.bounds
        fromView.frame = container.bounds
        container.addSubview(transitioningView)
        if isPresenting {
            toView.transform = CGAffineTransform.identity.translatedBy(x: 200, y: 0)
        }
        transitioningView.frame = self.originRect
        
        toView.layoutIfNeeded()
        
        fromTransitioningVc.prepareForTransition(presenting: isPresenting)
        toTransitioningVc.prepareForTransition(presenting: isPresenting)
        (transitioningView as? NavigationTransitioningView)?.prepareForTransition(presenting: isPresenting)
        
        // Standard animation
        UIView.animate(withDuration: self.duration, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
            guard let `self` = self else { return }
            doAnimations()
        }) { [weak self] (completed) in
            guard let `self` = self else { return }
            doCompletion(completed)
        }
        
        func doAnimations() {
            transitioningView.frame = self.targetRect
            fromTransitioningVc.animateInTransition(presenting: self.isPresenting)
            toTransitioningVc.animateInTransition(presenting: self.isPresenting)
            (transitioningView as? NavigationTransitioningView)?.animateInTransition(presenting: self.isPresenting)
            if !self.isPresenting {
                fromView.transform = CGAffineTransform.identity.translatedBy(x: 200, y: 0)
            } else {
                toView.transform = CGAffineTransform.identity
            }
        }
        
        func doCompletion(_ completed: Bool) {
            
            let transitionCompleted = !transitionContext.transitionWasCancelled
            if transitionCompleted {
                self.receivingView.receive(transitionedView: self.transitioningView)
            } else {
                self.givingView.receive(transitionedView: self.transitioningView)
            }
            
            fromTransitioningVc.endTransition(presenting: self.isPresenting, completed: transitionCompleted)
            toTransitioningVc.endTransition(presenting: self.isPresenting, completed: transitionCompleted)
            (transitioningView as? NavigationTransitioningView)?.endTransition(presenting: self.isPresenting, completed: transitionCompleted)
            
            toView.transform = CGAffineTransform.identity
            fromView.transform = CGAffineTransform.identity
            
            transitionContext.completeTransition(transitionCompleted)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
}
