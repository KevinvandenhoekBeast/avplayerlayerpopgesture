//
//  NavigationController.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    private var navigationInteractor: NavigationInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        delegate = self
    }
}

extension NavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let receivingView = toVC as? NavigationTransitioningView,
            let givingView = fromVC as? NavigationTransitioningView,
            let targetRect = receivingView.getTargetRect(),
            let originRect = givingView.getTargetRect(),
            let transitioningView = givingView.getTransitioningView(),
            receivingView.allows(view: transitioningView)
            else { return nil }
        
        let duration: CGFloat = 0.5
        if operation == .push {
            navigationInteractor = NavigationInteractor(attachTo: toVC)
        }
        return NavigationAnimator(duration: duration, isPresenting: operation == .push, originRect: originRect, targetRect: targetRect, transitioningView: transitioningView, givingView: givingView, receivingView: receivingView, navigationController: self)
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let interactor = navigationInteractor else { return nil }
        if interactor.transitionInProgress {
            return navigationInteractor
        } else {
            return nil
        }
    }
}
