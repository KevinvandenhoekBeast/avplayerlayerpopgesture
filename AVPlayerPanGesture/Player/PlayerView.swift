//
//  PlayerView.swift
//  AVPlayerPanGesture
//
//  Created by Kevin van den Hoek on 19/07/2018.
//  Copyright © 2018 Triple. All rights reserved.
//

import UIKit
import AVFoundation

class PlayerView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        player = AVPlayer()
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        player = AVPlayer()
        setup()
    }
    
    var player: AVPlayer {
        get {
            return playerLayer.player!
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    // TODO: The AVPlayerLayer layerClass completely messes up the pop gesture when combined with animating its frame
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    func setup() {
        playerLayer.videoGravity = .resizeAspectFill
        guard let url = URL(string: "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4") else { return }
        play(url: url)
        self.isUserInteractionEnabled = false
        self.backgroundColor = UIColor(red: 0.3, green: 0.5, blue: 0.5, alpha: 1)
    }
    
    private func play(url: URL) {
        let item = AVPlayerItem.init(url: url)
        player.replaceCurrentItem(with: item)
        player.play()
    }
}
